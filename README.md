
Este sistema ha sido creado con NodeJS Express, Swagger and MongoDB.

Para ellos es necesario instalar MongoDB 3.6, luego se debe crear una carpeta vacia llamada Data, a partir de este momento se puede iniciar mongo,
con el comando mongod en la carpeta C:\Program Files\MongoDB\Server\3.6\bin

Para administrar la base de datos se sugiere instalar robomongo, luego crear la conexion en el mismo.

Para restaurar la base de datos, se puede realizar un ejecutable con lo siguiente:

CD C:\Program Files\MongoDB\Server\3.6\bin
mongo encuestadb --eval "db.dropDatabase()"
mongorestore --db encuestadb C:\rutadelarchivodb

Para iniciar la consola de las Api, se debe posicionar en la carpeta donde se encuentre el programa ejemplo(C:\Test\test\sistema-api>)
instalar los package con npm install y luego para iniciar el servidor de api se coloca el comando swagger project start

Despues de esto se pueden consumir los servicios por postman o por la siguiente URL http://127.0.0.1:10010/api-docs/#/