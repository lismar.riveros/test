'use strict';

var mongoose = require('mongoose');

var answersSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
        idAnswer: Number,
        idQuestion: Number,
        idSurvey: Number,
        answers:String ,
        value:Number
    },{versionKey: false }
);

var answers = mongoose.model('answers',answersSchema);
module.exports = answers;