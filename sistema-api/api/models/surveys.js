'use strict';

var mongoose = require('mongoose');

var surveysSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
      
        idSurvey: Number,
        idProduct: Number,
        appliedSurveys: Number
    },{versionKey: false }
);

var surveys = mongoose.model('surveys',surveysSchema);
module.exports = surveys;