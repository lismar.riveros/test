'use strict';

var mongoose = require('mongoose');

var surveyresponsesSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
        idResponse: Number,
        idSurvey: Number,
        idQuestion: Number,
        idAnswer:Array 
    },{versionKey: false }
);

var surveyresponses = mongoose.model('surveyresponses',surveyresponsesSchema);
module.exports = surveyresponses;