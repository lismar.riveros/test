'use strict';

var mongoose = require('mongoose');

var productsSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
        idCompany: Number,
        idProduct: Number,
        idSurvey: Number,
        name:String 
    },{versionKey: false }
);

var products = mongoose.model('products',productsSchema);
module.exports = products;