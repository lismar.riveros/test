'use strict';

var mongoose = require('mongoose');

var companiesSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
        idCompany: Number,
        idSurvey: Number,
        name:String 
    },{versionKey: false }
);

var companies = mongoose.model('companies',companiesSchema);
module.exports = companies;