'use strict';

var mongoose = require('mongoose');

var questionsSchema = new mongoose.Schema(
    {
       // _id: mongoose.Schema.Types.ObjectId,
        idQuestion: Number,
        idSurvey: Number,
        question:String 
    },{versionKey: false }
);

var questions = mongoose.model('questions',questionsSchema);
module.exports = questions;