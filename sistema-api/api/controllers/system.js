'use strict';

var util = require('util');

var surveyresponses = require('../models/surveyresponses');
var products = require('../models/products');
var companies = require('../models/companies');
var surveys = require('../models/surveys');
var answers = require('../models/answers');


module.exports = {
  getAverage: getAverage,
  getLessMarked:getLessMarked,
  getList:getList
};

function getAverage(req, res) {

     surveyresponses.aggregate([

            {$match: {$and: [{"idAnswer":3},{"idQuestion":5}]}},
            
             {
                $lookup:
                    {
                        from: "products",
                        localField: "idSurvey",
                        foreignField: "idSurvey",
                        as: "products"
                    }
            },
             {
                $project: {
                    "_id": 0,
                    "idSurvey": "$products.idSurvey",
                    "name": "$products.name",
                    "idProduct":"$products.idProduct",
                    "idCompany":"$products.idCompany"
                     
                }
            },
            
            {
                $lookup:
                    {
                        from: "companies",
                        localField: "idSurvey",
                        foreignField: "idSurvey",
                        as: "companies"
                    }
            },
            {$unwind:{ "path": "$companies","preserveNullAndEmptyArrays": true}},
            
            {
                $project: {
                    "_id": 0,
                    "idSurvey": 1,
                    "idProduct":1,
                    "idCompany":1,
                    "name": "$companies.name"
                     
                }
            },
            
             {
                $lookup:
                    {
                        from: "surveys",
                        localField: "idSurvey",
                        foreignField: "idSurvey",
                        as: "surveys"
                    }
            },
            
             {$unwind:{ "path": "$surveys","preserveNullAndEmptyArrays": true}},
             {
                $project: {
                    "_id": 0,
                    "idSurvey": 1,
                    "idProduct":1,
                    "idCompany":1,
                    "name": 1,
                    "appliedSurveys":"$surveys.appliedSurveys"
                     
                }
            },
            
            { $group: { _id: "$name",   
                        count: { $sum: 1 },
                        quantity: { $avg: "$appliedSurveys" }
                       }
             },
            {
                $project: {
                    "_id": 1,
                    "count": "$count",
                    "Quantity":1,
                    "average": {$multiply: [ {$divide: ["$count","$quantity"]},100]}
                     
                }
            }]).exec(function (err, rows) {

            if (err) {
                res.json('error');
            } else {
                res.json(rows);
            }
        });
}

function getLessMarked(req, res) {
 
     surveyresponses.aggregate([
          {
              $lookup:
                  {
                      from: "products",
                      localField: "idSurvey",
                      foreignField: "idSurvey",
                      as: "products"
                  }
          },
                   {$unwind:{ "path": "$products","preserveNullAndEmptyArrays": true}},
           {
              $project: {
                  "_id": 0,
                  "idSurvey": "$products.idSurvey",
                  "nameProduct": "$products.name",
                  "idProduct":"$products.idProduct",
                  "idCompany":"$products.idCompany",
                  "idAnswer":1,
                  "idQuestion":1
                   
              }
          },
          
          {
              $lookup:
                  {
                      from: "companies",
                      localField: "idSurvey",
                      foreignField: "idSurvey",
                      as: "companies"
                  }
          },
          {$unwind:{ "path": "$companies","preserveNullAndEmptyArrays": true}},
          
          {
              $project: {
                  "_id": 0,
                  "idSurvey": 1,
                   "nameProduct":1,
                  "idProduct":1,
                  "name": "$companies.name",
                  "idAnswer":1
              
              }
          }
            ,{"$group" : {_id:{nameProduct:"$nameProduct",name:"$name","idProduct":"$idProduct"}, idAnswer: {$push: "$idAnswer"}}}
       
          ]).exec(function (err, rows) {

          if (err) {
              res.json('error');
          } else {

             products.find({},{
              _id: false,
              idProduct: 1,
              idCompany:1
             }, function (err, row) {
                
                 var todo=[];

                 for(var i=0;i<row.length;i++){
                    
                    var product;
                  
                    for(var j=0;j<rows.length;j++){
                      
                        if (row[i].idProduct === rows[j]._id.idProduct){       
                           var result=[];
                            result=result.concat(rows[j].idAnswer);
                            product= rows[j]._id.nameProduct;
                        
                                var contador = 0;
                                var repetidos = [];
                                var norepetidos=[];
                                var total_numeros = result.length;
                                var contadorant =0;
                                var menosrepetido=[];
                             
                                for (var m = 0; m < total_numeros; m++) {
                                  for (var c = 0; c < total_numeros; c++) {
                                    if (parseInt(result[m]) == parseInt(result[c])) {
                                      contador++;
                                    }
                                  }
                                 
                                  if (contador > 1 && repetidos.indexOf(parseInt(result[m])) === -1) {
                                    repetidos.push(parseInt(result[m]));
                                    if (contadorant===0){
                                      contadorant= contador;
                                      menosrepetido.push(parseInt(result[m]));
                                    }else if (contador<contadorant){
                                      contadorant= contador;
                                      menosrepetido.push(parseInt(result[m]));
                                    }

                                  }else if (contador===1 && repetidos.indexOf(parseInt(result[m])) === -1){
                                     norepetidos.push(parseInt(result[m]));
                                  }
                                  contador = 0;
                                }
                            
                                  if(norepetidos.length>0){
                                    todo.push('La opcion menos marcada para el producto '+product + ' es: '+norepetidos);
                                  }else{
                                      todo.push('La opcion menos marcada para el producto '+product + ' es: '+menosrepetido);
                                  }
      
                        }
   
                    }
                   
                  }

                 if (todo.length>0){
                    res.json(todo);
                 }else{
                  res.json('Todas las opciones han sido marcadas')
                 }

            });  

          }
      });
}


function getList(req, res) {

  surveyresponses.aggregate([

       {
          $lookup:
              {
                  from: "products",
                  localField: "idSurvey",
                  foreignField: "idSurvey",
                  as: "products"
              }
      },
               {$unwind:{ "path": "$products","preserveNullAndEmptyArrays": true}},
       {
          $project: {
              "_id": 0,
              "idSurvey": "$products.idSurvey",
              "nameProduct": "$products.name",
              "idProduct":"$products.idProduct",
              "idCompany":"$products.idCompany",
              "idAnswer":1,
              "idQuestion":1
               
          }
      },
      
      {
          $lookup:
              {
                  from: "companies",
                  localField: "idSurvey",
                  foreignField: "idSurvey",
                  as: "companies"
              }
      },
      {$unwind:{ "path": "$companies","preserveNullAndEmptyArrays": true}},
      
      {
          $project: {
              "_id": 0,
              "idSurvey": 1,
               "nameProduct":1,
              "idProduct":1,
              "name": "$companies.name",
              "idAnswer":1,
                "idQuestion":1
          
          }
          
      },
       {
          $lookup:
              {
                  from: "answers",
                  localField: "idSurvey",
                  foreignField: "idSurvey",
                  as: "answers"
              }
      },
      
      {
          $project: {
              "_id": 0,
              "idSurvey": 1,
               "nameProduct":1,
              "idProduct":1,
              "name": "$companies.name",
              "idAnswer":1,
               "idQuestion":1,
              "value":{
                  
                  "$map": {
                    "input": {
                      "$filter": {
                        "input": "$answers",
                        "as": "answers",
                        "cond": { 
                          "$and": [
                            { "$eq": [ "$$answers.idQuestion", "$idQuestion" ] },
                            { "$in": [ "$$answers.idAnswer", "$idAnswer" ] }
                          ]
                        }
                      }
                    },
                    
                      "as": "values",
                      "in":  "$$values.value"   
                }
            }
           
            
          }
          
      },
      
       {
          $project: {
              "_id": 0,
              "idSurvey": 1,
              "nameProduct":1,
              "idProduct":1,
              "name": 1,
              "idAnswer":1,
               "idQuestion":1,
               "value":{ $sum: "$value" }
          }
      },
      
      {"$group" : 
           {_id:{nameProduct:"$nameProduct",name:"$name"}, value:{ $sum: "$value" }}
       },
         {
            $sort:{value:-1}
          }
      
      ]).exec(function (err, row) {

          if (err) {
              res.json('error');
          } else {
              
            res.json(row);
          }
        })

}